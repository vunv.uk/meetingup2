const styles = {
  container: {
    flex: 1,
    flexDirection: 'column',
    margin: 15,
  },
};

export default styles;