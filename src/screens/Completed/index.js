import React, { Component } from 'react';
import { View, Text, ScrollView, Button, FlatList } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import EventCompleted from '../../components/Eventcompleted';
import styles from './styles';
import { database } from '../../config/firebase';
import { fetchEvents } from '../../actions/events';

class Completed extends Component {

  static navigationOptions = {
    title: 'Completed'
  }

  componentDidMount() {
    const { fetchEvents } = this.props;
    fetchEvents();
  }

  render() {
    const { events } = this.props;
    const listEvents = events.result;
    return (
      <View style={styles.container}>
        {
          listEvents && (
            <FlatList
              data={listEvents}
              renderItem={({ item, index }) => (
                <EventCompleted
                  eventsDataFirebase={item}
                />
              )}
              extraData={listEvents}
              keyExtractor={(item, index) => item.date}
              ItemSeparatorComponent={this.renderSeparator}
            />
          )
        }
      </View>
    );
  }
}

export default connect(
  state => ({ events: state.events.eventInfos }),
  dispatch => bindActionCreators({ fetchEvents }, dispatch)
)(Completed);
