const styles = {
  container: {
    flex: 1,
    backgroundColor: '#ff9966',
  },
  //Block 1
  backBtn: {
    margin: 8,
  },
  title: {
    flex: 1,
    marginTop: 35
  },
  textTitle: {
    textAlign: 'center',
    fontSize: 32,
    color: '#fff',
    paddingBottom: 32,
    fontFamily: 'Lobster-Regular'
  },

  //Block 2
  inputContent: {
    flex: 3,
    alignItems: 'center',
    flexDirection: 'column',
    paddingLeft: 32,
    paddingRight: 32
  },
  input: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 5,
  },
  icon: {
    padding: 14,
  },
  textInput: {
    width: '100%',
    color: '#fff',
    fontSize: 16,
    padding: 8
  },
  
  //Block3
  register: {
    flex: 1,
    paddingLeft: 32,
    paddingRight: 32,
  },
  btnRegister: {
    borderColor: '#fff',
    borderWidth: 1,
    borderRadius: 8,
    width: '100%'
  },
  registerText: {
    textAlign: 'center',
    color: '#fff',
    padding: 10,
    fontSize: 18,
  }
};

export default styles;