import React, { Component } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  Alert
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { register } from '../../actions/user';

import styles from './styles';

class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    };
  }

  registerClick() {
    const { email, password } = this.state;
    const { register } = this.props;

    if ((/@gmail\.com$/.test(email)) || (/@smartdev\.vn$/.test(email))) {
      register(email, password).then(() => {
        const { userInfos } = this.props;
        if (userInfos.result !== null) {
          const resetAction = NavigationActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'Login' })],
          });
          this.props.navigation.dispatch(resetAction);
        } else {
          const error = userInfos.error;
          var errorCode = error.code;
          var errorMessage = error.message;
          Alert.alert(
            errorCode,
            errorMessage,
          )
        }
      })
    } else {
      Alert.alert(
        'Alert Title',
        'Ban phai nhap dung dinh dang gmail.com va smartdev.vn',
        [
          { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
          { text: 'OK', onPress: () => console.log("OK Pressed") },
        ],
        { cancelable: false }
      );
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
          <Image source={require('../../assets/images/back/back.png')}
            style={styles.backBtn} />
        </TouchableOpacity>

        <View style={styles.title}>
          <Text style={styles.textTitle}>Sign up with meetingup</Text>
        </View>

        <View style={styles.inputContent}>
          <View style={styles.input}>
            <Image source={require('../../assets/images/gmail/gmail.png')}
              style={styles.icon} />
            <TextInput placeholder={'Email'}
              underlineColorAndroid={'#fff'}
              placeholderTextColor={'#fff'}
              keyboardType={'email-address'}
              style={styles.textInput}
              onChangeText={(email) => this.setState({ email })} />
          </View>

          <View style={styles.input}>
            <Image source={require('../../assets/images/password/password.png')}
              style={styles.icon} />
            <TextInput placeholder={'Password'}
              underlineColorAndroid={'#fff'}
              placeholderTextColor={'#fff'}
              secureTextEntry={true}
              style={styles.textInput}
              onChangeText={(password) => this.setState({ password })} />
          </View>
        </View>

        <View style={styles.register}>
          <TouchableOpacity onPress={() => this.registerClick()}
            style={styles.btnRegister}>
            <Text style={styles.registerText}>Register</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default connect(
  state => ({ userInfos: state.user.userInfos }),
  dispatch => bindActionCreators({
    register
  }, dispatch)
)(Signup);