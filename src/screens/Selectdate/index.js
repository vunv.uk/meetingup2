import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity
} from 'react-native';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
import moment from 'moment';
import styles from './styles';

export default class Selectdate extends Component {
  constructor(props) {
    super(props)
    this.state = {
      date: moment(new Date()).format('YYYY-MM-DD')
    }
  }

  static navigationOptions = {
    header: null,
    title: '+',
  }
  
  setTime = (day) => {
    this.setState({ date: day.dateString });
    this.props.navigation.navigate('Selecttime', { day })
  }

  showContent() {
    <TouchableOpacity style={styles.select}
      onPress={() => this.props.navigation.goBack()}>
      <Image style={{ opacity: 0.5 }} source={require('../../assets/images/arrowLeft/arrowLeft.png')} />
      <Text style={styles.selectDay}> Select day </Text>
    </TouchableOpacity>
  }

  calendarEvent = () => {
    const vacation = {
      key: 'vacation',
      color: 'white',
      selectedDotColor: 'white'
    };
    const workout = {
      key: 'workout',
      color: 'white',
      selectedDotColor: 'white'
    };

    return (
      <Calendar
        onDayPress={day => { this.setTime(day) }}
        markedDates={{
          [this.state.date]: {
            dots: [vacation, workout],
            selected: true,
            marked: [this.state.date],
            selectedColor: '#ff5e62'
          },
        }}
        markingType={'multi-dot'}
        style={styles.calenders}
        theme={{
          arrowColor: 'black',
        }} />
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.calendersContent}>
          {this.showContent()}
          {this.calendarEvent()}
        </View>
      </View>
    );
  }
}