import { Dimensions } from 'react-native';

var { width, height } = Dimensions.get('window');

const styles = {
  container: {
    flex: 1,
    backgroundColor: '#fff',
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10
  },
  calendersContent: {
    margin: 10,
  },
  select: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    padding: 10
  },
  selectDay: {
    color: '#6b6b6b',
    fontWeight: 'bold',
    fontSize: 16,
    bottom: 3
  },
};

export default styles;