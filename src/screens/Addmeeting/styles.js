const styles = {
  wrapper: {
    flex: 1,
    margin: 15,
    display: 'flex',
  },

  content: {
    backgroundColor: '#ffffff',
    padding: 10,
    height: 500
  },

  heading: {
    fontSize: 15,
    color: '#000000',
    marginBottom: 10,
    fontWeight: 'bold'
  },

  nextCircle: {
    borderRadius: 50,
    padding: 20,
    width: 40,
    height: 40,
    backgroundColor: '#ff5a63',
    borderWidth: 0,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'flex-end',
  },

}

export default styles;