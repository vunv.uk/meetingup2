import React, { Component } from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image
} from 'react-native';
import styles from './styles';

class Meeting extends Component {

  static navigationOptions = {
    header: null,
    title: '+',
  }

  render() {
    return (
      <View style={styles.wrapper}>
        <View style={styles.content}>
          <Text style={styles.heading}>New Meeting</Text>
          <TextInput
            placeholder="Name of Meeting"
            maxLength={100} autoFocus={true}
          />
          <TextInput
            placeholder="Address"
            maxLength={100}
          />
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Invite')}>
            <View style={styles.nextCircle}>
              <Image source={require('../../assets/images/next/next.png')} />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

}

export default Meeting;

