import React, { Component } from 'react';
import { View, Text, TouchableOpacity, FlatList, ScrollView } from 'react-native';

import styles from './styles';

class Summary extends Component {

  constructor(props) {
    super(props);
    this.state = {
      events: [
        { Date: "12 Aug 2017", Event: "Pool Party", Address: "245 Hung Vuong st", Time: "16:00- 17:00" }
      ],
      people: [
        { Name: 'Olenna Tyrell', Email: 'olennatyrell@highgarden.com' },
        { Name: 'Margrey Tyrell', Email: 'maryeolennatyrell@highgarden.com' },
        { Name: 'Olenna Tyrell', Email: 'olennatyrell@highgarden.com' },
        { Name: 'Olenna Tyrell', Email: 'olennatyrell@highgarden.com' },
      ]
    }
  }

  static navigationOptions = {
    header: null,
    title: '+',
  }
  
  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.top}>
            <Text style={styles.title}>New conference call added</Text>
            <FlatList
              data={this.state.events}
              renderItem={({ item, index }) => (
                <View>
                  <Text style={styles.eventname}>{item.Event}</Text>
                  <View style={styles.datetime}>
                    <Text style={styles.date}>{item.Date}</Text>
                    <Text style={styles.time}>{item.Time}</Text>
                  </View>
                </View>
              )}
              keyExtractor={item => item.Event}
              ItemSeparatorComponent={this.renderSeparator}
            />
          </View>
          <View>
            <Text style={styles.title}>Invites have been sent to</Text>
            <FlatList
              data={this.state.people}
              renderItem={({ item, index }) => (
                <View style={styles.people}>
                  <Text style={styles.peoplename}>{item.Name}</Text>
                  <Text>{item.Email}</Text>
                </View>
              )}
              keyExtractor={item => item.Name}
              ItemSeparatorComponent={this.renderSeparator}
            />
          </View>
          <TouchableOpacity style={styles.editdetail}>
            <Text>Edit Details</Text>
          </TouchableOpacity>
          <View style={styles.btn}>
            <TouchableOpacity style={styles.btnMain} onPress={() => this.props.navigation.navigate('Upcoming')}>
                <Text style={styles.btnText}>Set Remainder</Text>          
            </TouchableOpacity>
            <TouchableOpacity style={styles.btnMain} onPress={() => this.props.navigation.navigate('Upcoming')}>
                <Text style={styles.btnText}>No thanks</Text>        
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    )
  }
}

export default Summary;
