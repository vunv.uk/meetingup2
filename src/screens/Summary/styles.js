const styles = {
  container: {
    backgroundColor: '#ffffff',
    margin: 15,
    flex: 1,
    flexDirection: 'column',
    padding: 15,
  },
  title: {
    fontSize: 15,
    color: 'black',
    marginTop: 30,
    marginBottom: 20,
  },
  eventname: {
    fontSize: 17,
    color: 'black',
    fontWeight: 'bold',
    marginBottom: 10,
  },
  datetime: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#d6d7da',
    paddingBottom: 20
  },
  date: {
    color: '#ff6b6f',
    marginRight: 20
  },
  time: {
    color: '#ff6b6f',
  },
  peoplename: {
    color: 'black',
    paddingBottom: 5
  },
  people: {
    borderBottomWidth: 1,
    borderBottomColor: '#d6d7da',
    paddingBottom: 10,
    paddingTop: 10
  },
  editdetail: {
    paddingTop: 13,
    paddingBottom: 20
  },
  btn: {
    justifyContent: 'center',
    flex: 1,
    backgroundColor: '#ff6b6f',
  },
  btnMain: {
    backgroundColor: '#ffffff',
    flex: 2,
    justifyContent: 'center'
  },
  btnText: {
    textAlign: 'center',
    color: '#ff6b6f',
    padding: 10,
    fontSize: 13,
    fontWeight: 'bold'
  },
}
export default styles;