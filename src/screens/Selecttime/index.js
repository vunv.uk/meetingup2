import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet
} from 'react-native';
import { Agenda } from 'react-native-calendars';
import styles from './styles';
import moment from 'moment';

export default class Selecttime extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: {},
      date: moment().format('YYYY-MM-DD')
    };
  }

  static navigationOptions = {
    header: null,
    title: '+',
  }
  
  prevMonth(month) {
    month = this.state.date.month - 1
  }

  nextMonth(month) {
    month = this.state.date.month + 1
  }

  render() {
    const { params } = this.props.navigation.state;
    const day = params ? params.day : null;

    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.select}
          onPress={() => this.props.navigation.goBack()}>
          <Image style={{opacity: 0.5}} source={require('../../assets/images/arrowLeft/arrowLeft.png')} />
          <Text style={styles.selectDay}> Select time </Text>
        </TouchableOpacity>

        <View style={styles.month}>
          <Text style={styles.monthText}>{moment(day).format('MMM')}</Text>

          <TouchableOpacity style={styles.prevMonth}
            onPress={this.prevMonth.bind(this)}>
            <Image source={require('../../assets/images/arrowLeft/arrowLeft.png')} />
          </TouchableOpacity>

          <TouchableOpacity style={styles.nextMonth}
            onPress={this.nextMonth.bind(this)}>
            <Image source={require('../../assets/images/arrowRight/arrowRight.png')} />
          </TouchableOpacity>
        </View>

        <Agenda
          items={this.state.items}
          selected={day}
          loadItemsForMonth={this.loadItems.bind(this)}
          renderItem={this.renderItem.bind(this)}
          renderEmptyDate={this.renderEmptyDate.bind(this)}
          rowHasChanged={this.rowHasChanged.bind(this)}
          theme={customeTheme} />

        <View style={styles.nextCompelete}>
          <TouchableOpacity style={styles.nextBtn} onPress={() => this.props.navigation.navigate('Summary')}>
            <Image source={require('../../assets/images/next/next.png')} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  loadItems(day) {
    setTimeout(() => {
      for (let i = -15; i < 85; i++) {
        const time = day.timestamp + i * 24 * 60 * 60 * 1000;
        const strTime = this.timeToString(time);
        if (!this.state.items[strTime]) {
          this.state.items[strTime] = [];
          const numItems = Math.floor(Math.random() * 5);
          for (let j = 0; j < numItems; j++) {
            this.state.items[strTime].push({
              name: 'Item for ' + strTime,
              height: Math.max(50, Math.floor(Math.random() * 150))
            });
            this
          }
        }
      }

      const newItems = {};
      Object.keys(this.state.items).forEach(key => { newItems[key] = this.state.items[key]; });
      this.setState({
        items: newItems
      });
    }, 1000);
  }

  renderItem(item) {
    return (
      <View style={[styles.item, { height: item.height }]}>
        <Text>test thu</Text>
      </View>
    );
  }

  renderEmptyDate() {
    return (
      <View style={styles.emptyDate}>
        <Text>This is empty date!</Text>
      </View>
    );
  }

  rowHasChanged(r1, r2) {
    return r1.name !== r2.name;
  }

  timeToString(time) {
    const date = new Date(time);
    return date.toISOString().split('T')[0];
  }
}

const customeTheme = {
  'stylesheet.agenda.main': {
    header: {
      display: 'none'
    },
    weekday: {
      display: 'none'
    },
    reservations: {
      marginTop: 20,
      flex: 1
    }
  },
  'stylesheet.agenda.list': {
    container: {
      flexDirection: 'row',
      backgroundColor: '#f2f2f2'
    },
    day: {
      width: 63,
      alignItems: 'center',
      marginTop: 4,
      borderRightWidth: 4,
      borderRightColor: '#f2f2f2',
      backgroundColor: 'white',
      justifyContent: 'center',
    },
    dayNum: {
      fontSize: 18,
      color: '#bfbfbf',
      fontWeight: 'bold'
    },
    dayText: {
      fontSize: 14,
      color: '#bfbfbf',
      fontWeight: 'bold'
    },
    today: {
      color: 'white',
      backgroundColor: '#ff5e62',
      justifyContent: 'center',
      width: 70,
      flex: 1,
      borderRightWidth: 5,
      borderRightColor: '#dfe1e6',
      padding: 20,
    }
  },
}
