import { Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
const styles = {
  //Container
  container: {
    flex: 1,
    paddingTop: 10,
    paddingLeft: 10,
  },

  //Block 1
  select: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    backgroundColor: 'white',
    padding: 10,
  },
  selectDay: {
    color: '#6b6b6b',
    fontWeight: 'bold',
    fontSize: 16,
    bottom: 3
  },

  //Block 2
  month: {
    backgroundColor: 'white',
    flexDirection: 'row',
    padding: 10,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    bottom: 0,
  },
  monthText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#bfbfbf'
  },
  prevMonth: {
    opacity: 0.5,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    top: 4,
    left: 4
  },
  nextMonth: {
    opacity: 0.5,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    top: 4,
    left: 4
  },

  //Block 3
  agenda: {
    flex: 1,
    maxWidth: width,
    maxHeight: height,
    backgroundColor: 'white'
  },
  item: {
    backgroundColor: 'white',
    flex: 1,
    padding: 10,
    marginTop: 4,
  },
  emptyDate: {
    flex: 1,
    padding: 10,
    marginTop: 5,
  },

  //Block 4
  nextCompelete: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },

  nextBtn: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 60,
    position: 'absolute',
    right: 20,
    bottom: 20,
    padding: 20,
    backgroundColor: '#ff5a63'
  }
}

export default styles;
