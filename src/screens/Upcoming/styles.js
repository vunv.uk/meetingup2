const styles = {
  wrapper: {
    flex: 1,
    margin: 5,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    display: 'flex',
  },

  content: {
    padding: 15,
    width: 350,
    backgroundColor: '#ffffff',
  },

  sort: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderBottomColor: '#d6d7da',
    paddingBottom: 10
  },

  title: {
    fontSize: 17,
    fontWeight: 'bold',
    color: '#ff6666'
  },

  heading: {
    marginTop: 15,
    marginBottom: 8,
    fontSize: 16,
    color: '#000000'
  },

  joiner: {
    fontSize: 15,
  },

  time: {
    fontSize: 15,
    color: '#000000'
  },
}

export default styles;
