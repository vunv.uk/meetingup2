import React, { Component } from "react";
import { View, Text, FlatList } from "react-native";
import styles from './styles';

class Upcoming extends Component {

  constructor(props) {
    super(props);
    this.state = {
      datas: [
        [
          { key: 'Today', event: 'Pool Party', joiner: 'David, Beckham', time: '09:00-10:00' },
        ],
        [
          { key: 'Tomorrow', event: 'Dancing Sport', joiner: 'Thomas, Muller', time: '10:30-11:00' },
          { key: 'Tomorrow', event: 'Buffet', joiner: 'Sahra, Coner', time: '13:00-14:00' },
        ],
        [
          { key: '6 April 2018', event: 'Have Lunch', joiner: 'John, Cena', time: '10:30-11:00' },
          { key: '6 April 2018', event: 'Dancing Sport', joiner: 'Thomas, Muller', time: '10:30-11:00' },
          { key: '6 April 2018', event: 'Running Man', joiner: 'Sahra, Coner', time: '13:00-14:00' }
        ]
      ]
    }

  }

  renderDataRow = item => {
    return item.map((value, index) => {
      return (
        <View key={index}>
          <Text style={styles.heading}>{value.event}</Text>
          <View style={styles.sort}>
            <Text style={styles.joiner}>{value.joiner}</Text>
            <Text style={styles.time}>{value.time}</Text>
          </View>
        </View>
      )
    })
  }

  renderDisplay(item) {
    const val = item
    return (
      <View style={styles.content}>
        <Text style={styles.title}>{val[0].key}</Text>
        {this.renderDataRow(val)}
      </View>
    )
  }

  renderKeyExtractor = (item, index) => index;

  render() {
    return (
      <View style={styles.wrapper}>
        <FlatList
          data={this.state.datas}
          keyExtractor={this.renderKeyExtractor}
          renderItem={({ item }) => this.renderDisplay(item)}
        />
      </View>
    );
  }
}

export default Upcoming;