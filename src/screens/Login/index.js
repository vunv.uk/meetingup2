import React, { Component } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  Alert,
  ToastAndroid
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { GoogleSignin, GoogleSigninButton } from 'react-native-google-signin';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { signIn } from '../../actions/user';
import styles from './styles';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    }
  }

  componentWillMount() {
    try {
      GoogleSignin.hasPlayServices({ autoResolve: true });
      GoogleSignin.configure({
        webClientId: '694300334574-j20v114mio0k3btciaek2p89m9nf1qq9.apps.googleusercontent.com',
      });
    }
    catch (error) {
      var errorCode = error.code;
      var errorMessage = error.message;
      Alert.alert(
        errorCode,
        errorMessage,
      );
    }
  }

  handleSigninGoogle() {
    GoogleSignin.signIn().then(() => {
      this.props.navigation.navigate('MainNavigator');
    })
    .catch((err) => {
        console.log('error message', err.code, err.message);
    });
  }

  signInNormal() {
    const { email, password } = this.state;
    const { signIn } = this.props;

    signIn(email, password).then(() => {
      const { userInfos } = this.props;
      if (userInfos.result !== null) {
        const resetAction = NavigationActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'MainNavigator' })],
        });
        this.props.navigation.dispatch(resetAction);
      } else {
        const error = userInfos.error;
        var errorCode = error.code;
        var errorMessage = error.message;
        Alert.alert(
          errorCode,
          errorMessage,
        );
      };
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <View>
          <Text style={styles.title}>Sign in</Text>
          <Image source={require('../../assets/images/logo/logomeetings.png')} style={styles.logo} />
        </View>

        <View>
          <View style={styles.input}>
            <Image source={require('../../assets/images/email/email.png')} style={styles.iconUsername} />
            <TextInput placeholder={'Email'}
              underlineColorAndroid={'#fff'}
              placeholderTextColor={'#fff'}
              style={styles.textInput}
              onChangeText={(email) => this.setState({ email })}
            />
          </View>

          <View style={styles.input}>
            <Image source={require('../../assets/images/password/password.png')} style={styles.iconPassword} />
            <TextInput placeholder={'Password'}
              underlineColorAndroid={'#fff'}
              placeholderTextColor={'#fff'}
              secureTextEntry={true}
              onChangeText={(password) => this.setState({ password })}
              style={styles.textInput} />
          </View>

          <View style={styles.support}>
            <TouchableOpacity>
              <Text style={styles.forgotPass}>Forgot password?</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Signup')}>
              <Text style={styles.signUp}>Not member <Text style={styles.signUpBtn}>Sign Up</Text> free</Text>
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.button}>
          <TouchableOpacity onPress={() => this.signInNormal()}
            style={styles.btnLogin}>
            <Text style={styles.loginText}>Login</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.btnGoogle}
            onPress={() => this.handleSigninGoogle()}>
            <Text style={styles.textGoogle}>Sign in with Google + </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default connect(
  state => ({ userInfos: state.user.userInfos }),
  dispatch => bindActionCreators({
    signIn
  }, dispatch)
)(Login);