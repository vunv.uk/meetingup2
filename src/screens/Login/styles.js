const styles = {
  container: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'column',
    backgroundColor: '#ff9966',
    paddingLeft: 32,
    paddingRight: 32
  },

  //Block 1
  title: {
    paddingLeft: 8,
    fontSize: 32,
    color: '#fff',
    fontFamily: 'Lobster-Regular'
  },
  logo: {
    alignItems: 'center',
    width: 100,
    height: 100
  },

  //Block 2
  input: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 5,
  },
  iconUsername: {
    padding: 14,
  },
  iconPassword: {
    padding: 14,
  },
  textInput: {
    width: '100%',
    color: '#fff',
    fontSize: 16,
    padding: 8
  },
  support: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center'
  },
  forgotPass: {
    color: '#fff',
    fontSize: 16
  },
  signUp: {
    color: '#fff',
    fontSize: 16
  },
  signUpBtn: {
    fontWeight: 'bold'
  },

  //Block3
  button: {
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  btnLogin: {
    justifyContent: 'center',
    alignContent: 'center',
    borderRadius: 8,
    borderWidth: 1,
    borderColor: '#fff',
    width: 100
  },
  loginText: {
    color: '#fff',
    padding: 10,
    fontSize: 18,
    fontWeight: 'normal',
    textAlign: 'center'
  },
  btnGoogle: {
    justifyContent: 'center',
    alignContent: 'center',
    borderRadius: 8,
    borderWidth: 1,
    borderColor: '#fff',
  },
  textGoogle: {
    color: '#fff',
    padding: 10,
    fontSize: 18,
    fontWeight: 'normal',
    textAlign: 'center'
  }
};

export default styles;