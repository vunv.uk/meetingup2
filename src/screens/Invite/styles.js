const styles = {
  wrapper: {
    flex: 1, 
    margin: 5, 
    alignItems: 'center',
    justifyContent: 'center', 
    padding: 10,
    display: 'flex',     
  },

  content: {
    padding: 15,
    width: 350,
    backgroundColor: '#ffffff',
  },

  title: {
    fontSize: 17, 
    fontWeight: 'bold',
  },
  
  heading: {
    fontSize: 14, 
    color: '#000000'
  },

  circle: {
    borderRadius: 50,
    width: 20,
    height: 20,
    borderColor: '#00ffff',
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },

  nextCircle: {
    borderRadius: 50,
    width: 40,
    height: 40,
    backgroundColor: '#ff5a63',
    borderWidth: 0,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'flex-end'
  },

  addPeople: {
    color: '#00ffff'
  }, 

  backInvite: {
    flexDirection: 'row',
  },
  
  backIcon: {
    bottom: 1,
  },
  
  inviteText: {
    bottom: 3,
    fontWeight: 'bold',
  }
}

export default styles;
