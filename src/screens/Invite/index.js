import React, { Component } from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  FlatList,
  Image
} from 'react-native';
import styles from './styles';

class Invite extends Component {
  constructor(props) {
    super(props);
    this.state = {
      people: [
        { name: 'Jon Snow', email: 'Jonsnow@smartdev.com' },
        { name: 'Sansa Stark', email: 'Starksansa@smartdev.com' },
        { name: 'Tyller Belick', email: 'Tyller@smartdev.com' },
        { name: 'Eliot', email: 'Eliot@smartdev.com' },
        { name: 'Stanly', email: 'Stanly@smartdev.com' },
        { name: 'Eliot', email: 'Eliot@smartdev.com' }
      ]
    }
  }
  
  static navigationOptions = {
    header: null,
    title: '+',
  }

  render() {
    return (
      <View style={styles.wrapper}>
        <TouchableOpacity onPress={() => this.props.navigation.goBack()}  style={[styles.content, styles.backInvite]}>
          <Image source={require('../../assets/images/arrowLeft/arrowLeft.png')} style={styles.backIcon}/>
          <Text style={styles.inviteText}>
              Invite people
           </Text>
        </TouchableOpacity>
        <TextInput
          style={styles.content}
          placeholder="Email or phone number"
          maxLength={100}
        />
        <FlatList
          data={this.state.people}
          keyExtractor={item => item.name}
          ItemSeparatorComponent={this.renderSeparator}
          renderItem={({ item, index }) => (
            <View style={styles.content} key={index}>
              <TouchableOpacity>
                <Text style={styles.heading}>{item.name}</Text>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                  <Text>{item.email}</Text>
                  <View style={styles.circle}>
                    <Text style={styles.addPeople}>+</Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          )}
        />
        <TouchableOpacity onPress={() => this.props.navigation.navigate('Selectdate')}>
          <View style={styles.nextCircle}>
            <Image source={require('../../assets/images/next/next.png')} />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Invite;
