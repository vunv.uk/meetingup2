const styles = {
  container: {
    flex: 1,
    display: 'flex'
  },

  content: {
    padding: 15,
    backgroundColor: '#ffffff',
    borderBottomWidth: 1.5,
    borderBottomColor: '#d6d7da'
  },

  date: {
    fontSize: 17,
    fontWeight: 'bold',
    color: '#ff6666'
  },

  event: {
    marginTop: 15,
    marginBottom: 8,
    fontSize: 16,
    color: '#000000'
  },

  joiner: {
    fontSize: 15,
    width: 180
  },

  breakLineJoiner: {
    width: 180,
  },

  time: {
    fontSize: 15,
    color: '#000000'
  },

  sort: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
}

export default styles;