import React from 'react';
import { View, Text, TouchableOpacity, FlatList } from 'react-native';
import styles from './styles';

class EventCompleted extends React.Component {

  eventbyDate = eventDate => {
    let listJoiner = eventDate.joiner.map((joiner, index) => {
      return (
        <Text style={styles.joiner} key={index}>
          {joiner.name}
          {index !== eventDate.joiner.length - 1 && ', '}
        </Text>
      )
    });
    return (
      <TouchableOpacity>
        <View style={styles.content}>
          <Text style={styles.date}>{eventDate.date}</Text>
          <Text style={styles.event}>{eventDate.eventName}</Text>
          <View style={styles.sort}>
            <Text style={styles.breakLineJoiner}>
              {listJoiner}
            </Text>
            <Text style={styles.time}>{eventDate.time}</Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  render() {
    const { eventsDataFirebase } = this.props;
    return (
      <View style={styles.container}>
        {this.eventbyDate(eventsDataFirebase)}
      </View>
    );
  };
};

export default EventCompleted;