import { handleActions } from 'redux-actions';

const initialState = {
  userInfos: {
    requesting: false,
    result: null,
    error: null
  }
};

const user = handleActions({
  'SIGN_IN_REQUEST': (state) => {
    return {
      ...state,
      userInfos: {
        ...state.userInfos,
        requesting: true
      }
    }
  },

  'SIGN_IN_SUCCESS': (state, { payload }) => {
    return {
      ...state,
      userInfos: {
        ...state.userInfos,
        requesting: false,
        result: payload
      }
    }
  },

  'SIGN_IN_FAIL': (state, { payload }) => {
    return {
      ...state,
      userInfos: {
        ...state.userInfos,
        requesting: false,
        error: payload
      }
    }
  },



  'SIGN_UP_REQUEST': (state) => {
    return {
      ...state,
      userInfos: {
        ...state.userInfos,
        requesting: true
      }
    }
  },

  'SIGN_UP_SUCCESS': (state, { payload }) => {
    return {
      ...state,
      userInfos: {
        ...state.userInfos,
        requesting: false,
        result: payload
      }
    }
  },

  'SIGN_UP_FAIL': (state, { payload }) => {
    return {
      ...state,
      userInfos: {
        ...state.userInfos,
        requesting: false,
        error: payload
      }
    }
  }
}, initialState);

export default user;
