import { handleActions } from 'redux-actions';

const initialState = {
  eventInfos: {
    requesting: false,
    result: null,
    error: null
  },
}

const events = handleActions({
  'FETCH_EVENTS_REQUEST': (state) => {
    return {
      ...state,
      eventInfos: {
        ...state.eventInfos,
        requesting: true
      }
    }
  },
  'FETCH_EVENTS_SUCCESS': (state, { payload }) => {
    return {
      ...state,
      eventInfos: {
        ...state.eventInfos,
        requesting: false,
        result: payload.events
      }
    }
  },
  'FETCH_EVENTS_FAIL': (state, { payload }) => {
    return {
      ...state,
      eventInfos: {
        ...state.eventInfos,
        requesting: false,
        error: payload.error
      }
    }
  }
}, initialState);

export default events;