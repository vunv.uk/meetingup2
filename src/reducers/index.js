import { combineReducers } from 'redux';
import nav from './nav';
import events from './events';
import user from './user';


export default combineReducers({
  nav,
  events,
  user
});