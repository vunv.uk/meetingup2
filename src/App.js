import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BackHandler, Text } from 'react-native';
import { NavigationActions, addNavigationHelpers } from 'react-navigation';
import { connect } from 'react-redux';
import { addListener } from './middlewares/navigation';
import AppNavigator from './navigator';

@connect(
  state => ({
    nav: state.nav
  }),
  dispatch => ({ dispatch })
)
export default class AppWithNavigationState extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    nav: PropTypes.object.isRequired
  };

  componentDidMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.dispatch(NavigationActions.back());
      return true;
    });
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  render() {
    const { dispatch, nav } = this.props;

    return <AppNavigator navigation={{ dispatch, state: nav, addListener  }} />;
  }
}
