import React from 'react';
import PropTypes from 'prop-types';
import { AppRegistry, Text } from 'react-native';
import { Provider } from 'react-redux';

import App from './App';
import configureStore from './configureStore';

const store = configureStore();

const MeetingUp = () => (
  <Provider store={store}>
    <App />
  </Provider>
);

AppRegistry.registerComponent('meetingup', () => MeetingUp);
