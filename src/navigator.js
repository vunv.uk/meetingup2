import React from 'react';
import { StackNavigator, TabNavigator } from 'react-navigation';

import Completed from './screens/Completed';
import Login from './screens/Login';
import AddMeeting from './screens/Addmeeting';
import Upcoming from './screens/Upcoming';
import Signup from './screens/Signup';
import Invite from './screens/Invite';
import Selectdate from './screens/Selectdate';
import Selecttime from './screens/Selecttime';
import Summary from './screens/Summary';

const MainNavigator = new TabNavigator(
  {
    Upcoming: { screen: Upcoming },
    Completed: { screen: Completed },
    AddMeeting: {
      screen: StackNavigator(
        {
          AddMeeting: { screen: AddMeeting },
          Invite: { screen: Invite },
          Selectdate: { screen: Selectdate },
          Selecttime: { screen: Selecttime },
          Summary: { screen: Summary }
        },
        {
          headerMode: 'screen',
          navigationOptions: {
            header: null,
          }
        },
      )
    },
  },
  {
    tabBarPosition: 'top',
    swipeEnabled: true,
    animationEnabled: true,
    tabBarOptions: {
      activeTintColor: '#ff706b',
      inactiveTintColor: '#333333',
      upperCaseLabel: false,
      labelStyle: {
        fontSize: 15
      },
      style: {
        backgroundColor: '#ffffff',
      },
      indicatorStyle: {
        backgroundColor: '#ff706b'
      },
    }
  }
);

const AppNavigator = new StackNavigator(
  {
    Login: { screen: Login },
    Signup: { screen: Signup },
    MainNavigator: { screen: MainNavigator },
  },
  {
    headerMode: 'screen',
    navigationOptions: {
      header: null,
    },
  },
);


export default AppNavigator;
