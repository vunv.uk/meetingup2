import user from './user';
import events from './events';
export default {
  user,
  events
};