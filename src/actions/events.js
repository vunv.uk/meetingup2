import { createActions } from 'redux-actions';

import { database } from '../config/firebase';

const { fetchEventsRequest, fetchEventsSuccess, fetchEventsFail } = createActions({
  'FETCH_EVENTS_REQUEST': () => null,
  'FETCH_EVENTS_SUCCESS': (events) => ({ events }),
  'FETCH_EVENTS_FAIL': (error) => ({ error })
});

export const fetchEvents = () => dispatch => {
  dispatch(fetchEventsRequest());
  database.ref().on('child_added', snapshot => {
    if (snapshot.val() !== null) {
      dispatch(fetchEventsSuccess(snapshot.val()));
    } else {
      dispatch(fetchEventsFail(error));
    }
  })
}
