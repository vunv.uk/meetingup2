import { createActions } from 'redux-actions';
import { auth } from '../config/firebase';

const { signInRequest, signInSuccess, signInFail } = createActions({
  'SIGN_IN_REQUEST': () => null,
  'SIGN_IN_SUCCESS': (data) => data,
  'SIGN_IN_FAIL': (error) => error
});

export const signIn = (email, password) => dispatch => {
  dispatch(signInRequest());

  return auth.signInWithEmailAndPassword(email, password)
    .then((data) => {
      dispatch(signInSuccess(data));
    })
    .catch(function (error) {
      dispatch(signInFail(error));
    });
}


const { signUpRequest, signUpSuccess, signUpFail } = createActions({
  'SIGN_UP_REQUEST': () => null,
  'SIGN_UP_SUCCESS': (data) => data,
  'SIGN_UP_FAIL': (error) => error
});

export const register = (email, password) => dispatch => {
  dispatch(signUpRequest());

  return auth.createUserWithEmailAndPassword(email, password)
    .then((data) => {
      dispatch(signUpSuccess(data));
    })
    .catch(function (error) {
      dispatch(signUpFail(error));
    });
}
